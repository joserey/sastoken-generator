var crypto = require('crypto');

var host = ""; //Hostname, {your-iothub-name}.azure-devices.net
var device = ""; //You device
//any symmetric key for the {device id} identity
var signingKey = "";
var days = 365; //Valid days

var resourceUri = host + "/devices/" + device;

console.log(createSASToken(resourceUri, signingKey, null, days));

function createSASToken(resourceUri, signingKey, policyName, expiresDays) {

    resourceUri = encodeURIComponent(resourceUri);

    // Set expiration in seconds
    var expires = (Date.now() / 1000) + (expiresDays * 86400); //86400 = seconds in a day
    expires = Math.ceil(expires);
    var toSign = resourceUri + '\n' + expires;

    // Use crypto
    var hmac = crypto.createHmac('sha256', new Buffer(signingKey, 'base64'));
    hmac.update(toSign);
    var base64UriEncoded = encodeURIComponent(hmac.digest('base64'));

    // Construct autorization string
    var token = "SharedAccessSignature sr=" + resourceUri + "&sig=" +
        base64UriEncoded + "&se=" + expires;
    if (policyName) token += "&skn=" + policyName;
    return token;
}